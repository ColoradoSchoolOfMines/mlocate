#!/usr/bin/python3

import csv
import time
import requests
import json
import sys

identifier = sys.argv[1]
url = sys.argv[2] if len(sys.argv) > 2 else "http://34.73.196.176:8080/data"

file_name = 'testData-01.csv'

# url = "http://10.203.151.189:8080/data"
# url = "http://34.73.196.176:8080/data"


def parseFile():
    deviceInfo = {}

    with open(file_name) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')

        allow_reading = False

        for row in csv_reader:

            if row == []:
                continue

            if allow_reading:
                deviceInfo[row[0]] = row[3]
            if row[0] == "Station MAC":
                allow_reading = True

    return deviceInfo


def sendData(data):
    try:
        headers = {'Content-type': 'application/json'}
        r = requests.post(url, data=json.dumps(data), headers=headers)
        print("Request Code: ", r)
    except:
        pass
        

while True:
    data = parseFile()
    data['identifier'] = identifier
    sendData(data)
    time.sleep(5)
