#! /usr/bin/env python3

import math
import numpy as np
import itertools as it


def rotate(origin, point, angle):
    """
    Rotate a point counterclockwise by a given angle around a given origin.

    The angle should be given in radians.

    Source: https://stackoverflow.com/a/34374437/2319844
    """
    ox, oy = origin
    px, py = point

    qx = ox + math.cos(angle) * (px - ox) - math.sin(angle) * (py - oy)
    qy = oy + math.sin(angle) * (px - ox) + math.cos(angle) * (py - oy)
    return (qx, qy)


def trilaterate(AP_locations, distances):
    """
    Trilaterate a point in 2D space

    Technique from: https://stackoverflow.com/a/16176781/2319844
    """
    AP_locations = list(AP_locations)
    distances = list(distances)

    # Normalize everything.
    # =========================================================================
    # make sure that P1 is on the origin
    V = tuple(map(lambda x: -x, AP_locations[0]))
    AP_locations = list(
        map(lambda x: (x[0] + V[0], x[1] + V[1]), AP_locations))

    # rotate to make sure that P2 is on the x-axis
    theta = math.atan2(AP_locations[1][1], AP_locations[1][0])
    AP_locations = list(map(lambda p: rotate((0, 0), p, -theta), AP_locations))

    # Calculate the device position in the normalized coordinate plane.
    # =========================================================================
    d = AP_locations[1][0]  # the first x-coordinate of P1
    i, j = AP_locations[2]  # the coordinates of P2
    dev = ((distances[0]**2 - distances[1]**2 + d**2) / (2 * d), )
    dev += (((distances[0]**2 - distances[2]**2 + i**2 + j**2) / (2 * j)) - (
        (i / j) * dev[0]), )

    # Transform everything back.
    # =========================================================================
    x, y = rotate((0, 0), dev, theta)
    x, y = x - V[0], y - V[1]

    return (x, y)


def get_position(location_distance_map):
    """
    Trilaterate every combination of 3 points and calculates the average.

    Complexity: O(n * (n choose 3)), but who cares, it's a hackathon.
    """
    position_triplets = it.combinations(location_distance_map.items(), 3)

    # Calculate the results.
    results = [
        trilaterate(map(lambda x: x[0], pt), map(lambda x: x[1], pt))
        for pt in position_triplets
    ]

    return (
        sum(map(lambda x: x[0], results)) / len(results),
        sum(map(lambda x: x[1], results)) / len(results),
    )


if __name__ == '__main__':
    # Run the tests
    tests = [
        ([(20, 10), (20, 20), (10, 10)], [10.63, 7.28, 8.54], (13, 18), 0.01),
        ([(20, 10), (20, 20), (10, 10)], [10, 7, 9], (13, 18), 1.5),
        ([(20, 10), (20, 20), (10, 10), (10, 18)], [10, 7, 9, 3], (13, 18), 1),
        ([(20, 10), (20, 20), (10, 10), (10, 18)], [10, 7, 10, 2], (13, 18),
         1),
    ]

    for ap_loc, dists, expected, epsilon in tests:
        dev_loc = get_position(ap_loc, dists)

        print(dev_loc)
        assert np.allclose(dev_loc, expected, atol=epsilon)
