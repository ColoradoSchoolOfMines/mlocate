#!/usr/bin/env python3

from bottle import route, run, post, request, static_file
import re
import json
import math
import time
import random
from collections import defaultdict

from trilateration import get_position
from testDistance import strengthToDistance

data = defaultdict(dict)
startTime = time.time()

node_positions = {
    'A': (400, 700),
    'B': (700, 450),
    'C': (512, 180),
}
scaling_factor = 520 / 33

device_locations = {}

# Structure:
# A list inside of a dictionary inside of a dictionary
# {mac1 : {node1 : [time, strength], node2 : [time, strength]}}


@post('/data')
def recieve_data():
    jsonRaw = request.json
    identifier = jsonRaw['identifier']
    del jsonRaw['identifier']
    timestamp = time.time() - startTime

    for item in jsonRaw.items():  # For each device reported
        mac, strength = item
        strength = int(strength.strip())
        if strength == -1:  # discard, something was probably broken
            continue

        distance = strengthToDistance(strength)
        pingData = (timestamp, strength, distance)

        device = data[mac]
        device[identifier] = pingData

        if mac.lower() in [
                'bc:54:36:5e:08:55s', '3c:28:6d:e2:6c:8b', 'a0:c:2b:a3:fd:51',
                '34:36:3b:75:b6:4c', '0e:5f:3f:0e:1a:6a'
        ]:
            print('from', identifier)
            print(mac, strength, distance)

    for mac, ips in data.items():
        # If there are enough points, triangulate.
        if len(ips) >= 3:
            distances = {}

            for ip, pd in ips.items():
                distances[node_positions[ip]] = pd[2] * scaling_factor * 3

            print('asdf', get_position(distances))
            device_locations[mac] = get_position(distances)
            #device_locations[mac] = list(map(lambda z: z / scaling_factor, device_locations[mac]))
            print(device_locations[mac])

    print('Recieved {} MAC addresses\nTotal devices: {}'.format(
        len(jsonRaw), len(data)))


@route('/')
def index():
    with open('frontend/index.html') as f:
        return f.read()


@route('/resources/<filename:path>')
def resources(filename):
    return static_file(filename, root='frontend/resources')


@route('/retrieve_data')
def retrieve_data():
    return dict(
        devices=device_locations,
        nodes=list(node_positions.values()),
    )


run(host='0.0.0.0', port=8080, debug=True)
