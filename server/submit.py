import json, requests, random, time

# Put server ip here
ip = '10.203.151.189'
url = 'http://%s:8080/data' % ip

hexValues = [str(i) for i in range(10)] + ['a', 'b', 'c', 'd', 'e', 'f']
def randomMac(): # For making real-looking data:
    # strength = random.randint(-80, -50) # No
    return ':'.join([''.join([hexValues[random.randint(0, 15)] for i in range(2)]) for j in range(6)])


def submitData(macAddress, signalStrength): # Outdated
    macAddress = str(macAddress)
    signalStrength = str(signalStrength)
    
    data = {'MAC Address' : macAddress, 'Signal Strength' : signalStrength}
    
    headers = {'Content-type': 'application/json'}
    r = requests.post(url, data=json.dumps(data), headers=headers)
    print(r)

deviceCount = 100
updates = 100
devices = [randomMac() for i in range(deviceCount)]

startTime = time.time()

for i in range(updates):
    for j in range(deviceCount):
        mac = devices[j]
        strength = random.randint(-80, -50)
        print('Packet %d / %d, pass %d / %d:\t%s' % (j+1, deviceCount, i+1, updates, str([mac, strength])))
        submitData(mac, strength)

totalPackets = updates * deviceCount
print('Submitted %d packets, %.2f ms per packet' % (totalPackets, 1000*(time.time() - startTime)/totalPackets))