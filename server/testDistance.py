from math import exp, log10, sqrt

# frequency = 2400
# FSPL = 27.55
# #Something tossed together to test for signal loss
# def strengthToDistance(strength):
#    m = 10 ** ((FSPL - (20 * log10(frequency)) - strength) / 20)
#    return m

# # 3.3x too long at 1.6m

def strengthToDistance(strength):
    txpower=-59
    if strength == 0: return -1

    ratio = strength/txpower
    if ratio < 1:
        return pow(ratio, 10)
    else:
        return 0.89976 * pow(ratio, 7.7095) + 0.111
    return sqrt(2450/-strength)
    return 10**((27.55-(20*log10(2400)) + strength)/20)
    return 0.00297538 * 10**(-strength/20)
    return exp((-strength - 50) / 10)
    return 96 / 100 * -strength

# print(strengthToDistance(-55))
